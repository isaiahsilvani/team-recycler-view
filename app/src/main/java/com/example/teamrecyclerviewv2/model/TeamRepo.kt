package com.example.teamrecyclerviewv2.model

object TeamRepo {
    val TAG = "Teamrepo"
    // Set the teamList to empty within our local repository to get it started
    var teamList: MutableList<TeamModel> = mutableListOf()
    // This is supposed to be called inside a Coroutine...
    fun getTeamMembers(): List<TeamModel> {
        return teamList
    }
    fun addTeamMember(team: String,name: String) {
        val member = TeamModel(name, team)
        teamList.add(member)
    }
}