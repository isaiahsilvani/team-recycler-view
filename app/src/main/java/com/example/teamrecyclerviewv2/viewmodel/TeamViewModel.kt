package com.example.teamrecyclerviewv2.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.teamrecyclerviewv2.model.TeamModel
import com.example.teamrecyclerviewv2.model.TeamRepo
import com.example.teamrecyclerviewv2.view.ViewTeams.ViewTeamsState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TeamViewModel : ViewModel() {
    val TAG = "TeamViewModel"
    private val repo: TeamRepo = TeamRepo
    private val scope = CoroutineScope(Dispatchers.Default)

    // Update the ViewTeamsState with fresh data...
    private val _viewTeamsState: MutableLiveData<ViewTeamsState> = MutableLiveData(ViewTeamsState())
    val viewTeamsState: LiveData<ViewTeamsState> get() = _viewTeamsState

    fun handleSelection(selectedTeam: String, name: String) {
        with(_viewTeamsState) {
            // Update the ViewTeamsState
            var currentTeamList = value?.teamsList
            currentTeamList?.add(TeamModel(selectedTeam, name))
            value = value?.copy(teamsList = currentTeamList!!)
            Log.e(TAG, viewTeamsState.value.toString())
        }
        // add team member to local repository
        repo.addTeamMember(selectedTeam, name)
    }

    fun getTeamList(teamName: String): List<TeamModel> = repo.getTeamMembers().filter {
        it.team == teamName
    }

}