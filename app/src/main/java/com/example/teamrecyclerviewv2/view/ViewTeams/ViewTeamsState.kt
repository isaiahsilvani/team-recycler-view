package com.example.teamrecyclerviewv2.view.ViewTeams

import com.example.teamrecyclerviewv2.model.TeamModel

data class ViewTeamsState(
    var teamsList: MutableList<TeamModel> = mutableListOf()
)