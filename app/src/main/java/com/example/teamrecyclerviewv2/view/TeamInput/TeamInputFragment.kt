package com.example.teamrecyclerviewv2.view.TeamInput

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.teamrecyclerviewv2.databinding.FragmentTeamInputBinding
import com.example.teamrecyclerviewv2.viewmodel.TeamViewModel

class TeamInputFragment: Fragment() {
    val TAG = "TeamInputFragment"
    lateinit var binding: FragmentTeamInputBinding
    private val TeamVM by viewModels<TeamViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTeamInputBinding.inflate(layoutInflater)
        initViews()
        return binding.root
    }

    private fun initViews() {
        with(binding) {
            // When they click a button, handle the selection!
            val teamButtons = listOf(btnTeamA, btnTeamB)
            // If the user input is not empty and they click the team
            // buttons, add it to the repo
            for (button in teamButtons) {
                button.setOnClickListener {
                    val userInput = inputTeamsTextEdit.text.toString()
                    if (userInput.isNotEmpty()) {
                        val selection = button.text.toString()
                        TeamVM.handleSelection(selection, userInput)
                        inputTeamsTextEdit.text?.clear()
                    }
                }
            }
            // If user clicks view teams, navigate to the viewteams fragment
            btnViewTeams.setOnClickListener {
                Log.e(TAG, TeamVM.viewTeamsState.value.toString())
                val action = TeamInputFragmentDirections.actionTeamInputFragmentToViewTeamsFragment()
                findNavController().navigate(action)
            }
        }
    }
}