package com.example.teamrecyclerviewv2.view.ViewTeams

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.teamrecyclerviewv2.R
import com.example.teamrecyclerviewv2.model.TeamModel

class TeamAdapter: RecyclerView.Adapter<TeamAdapter.TeamViewHolder>() {
    val TAG = "TeamAdapter"
    var adapterList: List<TeamModel> = listOf()


    // onCreateViewHolder actually creates the ViewHolder for each item in the RecyclerView
    // data set
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamAdapter.TeamViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.team_member_item, parent, false)
        return TeamViewHolder(v)
    }
    // This ViewHolder class is responsible for telling the Adapter which
    // Views will eventually be binded by the given dataset
    inner class TeamViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var memberName: TextView
        var backgroundColor: ConstraintLayout

        init {
            memberName = itemView.findViewById(R.id.name_here)
            backgroundColor = itemView.findViewById(R.id.item_background)
        }
    }
    // onBindViewHolder is responsible for actually passing the data needed
    // for each instance of a ViewHolder, or item, into the predefined view
    // that we set for TeamViewHolder
    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.memberName.text = adapterList[position].name
        // if Team B, set the background color to blue
        if (adapterList[position].team == "Team B") {
            var color = holder.itemView.context.getColor(R.color.teal_200)
            holder.backgroundColor.background = ColorDrawable(color)
        }
    }
    // getItemCount is responsible for telling the adapter when to stop,
    // by checking if the position (Int) is == to teamList.size
    override fun getItemCount(): Int {
        return adapterList.size
    }

    fun setData(teamList: List<TeamModel>) {
        this.adapterList = teamList
    }
}