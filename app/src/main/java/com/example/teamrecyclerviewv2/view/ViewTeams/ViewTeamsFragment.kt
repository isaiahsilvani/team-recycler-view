package com.example.teamrecyclerviewv2.view.ViewTeams

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.teamrecyclerviewv2.R
import com.example.teamrecyclerviewv2.databinding.FragmentViewTeamsBinding
import com.example.teamrecyclerviewv2.model.TeamRepo
import com.example.teamrecyclerviewv2.viewmodel.TeamViewModel

class ViewTeamsFragment : Fragment() {
    val TAG = "ViewTeamsFragment"
    lateinit var binding: FragmentViewTeamsBinding

    //private val StartFragmentVM by viewModels<StartFragmentViewModel>()
    private val teamsVM by viewModels<TeamViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentViewTeamsBinding.inflate(layoutInflater)
        with(binding) {
            initViews()
            initObservers()
            // SET UP THE 1st RECYCLER ADAPTER
            recyclerView.layoutManager = GridLayoutManager(this@ViewTeamsFragment.context, 1)
            recyclerView.adapter = TeamAdapter().apply { setData(teamsVM.getTeamList(getString(R.string.team_a))) }
            // SET UP THE 2nd RECYCLER ADAPtER
            recyclerView2.layoutManager = GridLayoutManager(this@ViewTeamsFragment.context, 1)
            recyclerView2.adapter = TeamAdapter().apply { setData(teamsVM.getTeamList(getString(R.string.team_b))) }

            return root
        }
    }

    private fun initViews() {
        with(binding) {
            btnGoBack.setOnClickListener {
                val action =
                    ViewTeamsFragmentDirections.actionViewTeamsFragmentToTeamInputFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun initObservers() {
        teamsVM.viewTeamsState.observe(viewLifecycleOwner) {
            Log.e(TAG, it.teamsList.toString())
        }
    }
}
